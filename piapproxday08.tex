\documentclass[12pt,a4paper]{article}

\include{header}

\setlength{\oddsidemargin}{-5 mm}

\newtheorem{satz}{\begin{large}{Satz}\end{large}}[]

\hypersetup{pdfinfo={
  Title=Eine einsame Insel und die Zahl Pi,
  Author=Andreas Geyer-Schulz,
  Subject=Pi-Approximation Day 2008}
}

\begin{document}
\begin{center}
  {\huge Eine einsame Insel und die Zahl $\pi$}

  \vspace{5mm}
  {\Large 22. Juli 2008 -- {\em $\pi$-Approximation Day}}

  \vspace{5mm}
  {\large Andreas Geyer-Schulz}
\end{center}

Eine der wichtigsten Frage der Menschheit ist, welches Buch man sich auf eine einsame Insel mitnehmen würde, wenn man nur Platz für ein einziges Buch im Gepäck hätte.
Aus der Sicht eines Mathematikers wäre es toll, ein Buch zu haben, das \emph{alle} Stellen von $\pi$ auflistet, denn schließlich wird einem mit so einem Buch niemals langweilig, immer wieder kommen neue Zahlen. Doch leider hat selbst das dickste Buch mit der kleinsten Schrift nur endlich viel Platz und so ist es unvermeidbar, dass die letzten drei Zeichen auf der letzten Seite eines solchen \glqq $\pi$-Buches\grqq\ lauten \glqq $\ldots$\grqq.
Doch in der Mathematik darf man anders als im Alltag \glqq und so weiter\grqq\ dann und nur dann sagen, wenn man auch wirklich weiß wie  es weitergeht.

Aus diesem Grund möchte im Folgenden eine höchst unterhaltsame Methode vorstellen, wie man selbst auf einer einsamen Insel mit einfachsten Hilfsmitteln einen Näherungswert für $\pi$ ermitteln kann.
Dies hat zwei Vorteile:
Erstens wird einem dabei nicht fad und zweitens bleibt genügend Platz im Gepäck für ein richtiges Buch, da man ja das schwere \glqq $\pi$-Buch\grqq\ nicht mitschleppen muss.

Was braucht man also um $\pi$ anzunähern?
Einen wunderschönen Strand mit feinstem Sand und ein paar gleich lange, gerade Holzstecken, das wars!
Nun zeichnet man einige parallele Linien in den Sand, deren Abstand genauso groß sein soll, wie die Holzstecken lang sind.
Dann sucht man sich eine gemütliche Hängematte in unmittelbarer Nähe zu den Linien im Sand und wirft die Holzstecken willkürlich auf das Linienmuster.

Das Ergebnis schaut möglicherweise so aus:

\begin{center}
\includegraphics[height=52mm]{buffon01.eps}
\end{center}

Offensichtlich kreuzen manche der Staberln eine Linie, während andere in den Zwischenräumen liegenbleiben, und genau diese Beobachtung führt zu $\pi$!
Im Jahr 1777 fragte sich ein französischer Adeliger, Georges Louis Leclerc, Comte de Buffon, wie groß denn die Wahrscheinlichkeit
sei, dass ein Holzstäbchen\footnote{In der ursprünglichen Formulierung des Problems ging es um
Nadeln, die auf ein Blatt liniertes Papier geworfen werden.
Holzstäbchen am Strand sind zwar viel schöner, doch aus historischem Grund man nennt das Problem trotzdem \glqq Nadel-Problem.\grqq}
eine der Linien kreuzt.
Die Antwort ist genauso überraschend wie schön:
\vspace{1mm}

\hspace{-8mm}
\begin{tabular}{|p{418pt}|}
\hline
\begin{satz}
{\bf \glqq Das Nadel-Problem von Buffon\grqq}

Holzstäbchen der Länge $l$ werden auf parallele Linien im Sand geworfen, die den Abstand $l$ haben.
Dann ist die Wahrscheinlichkeit, dass ein Holzstäbchen so zu liegen kommt, dass es eine der Linien kreuzt, genau
\begin{equation*}
  p = \frac{2}{\pi}.
\end{equation*}
\end{satz} \\
\hline
\end{tabular}

\vspace{1mm}

Mit Hilfe dieses Satzes können wir nun ganz bequem von der Hängematte aus eine Näherung für $\pi$ errechnen.
Wir müssen nur zählen wie viele Holzstaberl wir geworfen haben und wieviele davon eine Linie getroffen haben.
Denn dieses wunderbare Ergebnis bedeutet nichts anderes, als dass wir bei $N$ geworfenen Holzstäbchen, von denen $P$ eine Linie kreuzen, erwarten dürfen, dass $\frac{P}{N}\approx \frac{2}{\pi}$ ist.
In anderen Worten, wenn wir $N$ Holzstäbchen werfen von denen $P$ eine Linie kreuzen, so erhalten wir einen Näherungswert für $\pi$, nämlich
\begin{equation*}
  \pi \approx \frac{2N}{P}.
\end{equation*}

\begin{proof}
Ein sehr eleganter, elementarer Beweis des Satzes findet sich in im Buch~\cite[Seite 153f.]{botp}, der ganz ohne Analysis auskommt.
Doch wenn man schon in der Schule mit Integralen gequält wurde, darf man sie ruhig auch verwenden!

Ohne Beschränkung der Allgemeinheit nehmen wir an, dass unsere Holzstäbchen die Länge $2$ haben, denn dass wird uns das Rechnen vereinfachen.
Dann überlegen wir uns, was bei unserem Zufallsexperiment überhaupt passieren kann.
Klar ist, dass es unendlich viele Ergebnisse gibt, die wir alle berücksichtigen müssen.
Wenn wir nun ein Stäbchen fallen lassen, können wir den Ausgang dieses Experiments dadurch beschreiben, dass wir den Abstand $x$ zwischen dem Mittelpunkt des Stäbchens und der nächste Linie messen, sowie den Winkel $\alpha$ indem das Stäbchen aufkommt.

\begin{center}
\includegraphics{buffon02.eps}
\end{center}

Offensichtlich können wir uns auf $0\leq x\leq 1$ und $0 \leq \alpha \leq \frac{\pi}{2}$ beschränken, da alle anderen
Ergebnisse dazu symmetrisch sind.
Anschaulich ist somit der Raum aller Möglichkeiten ein Rechteck mit Länge $\frac{\pi}{2}$ und Höhe $1$.
Im Bild ist das oben gezeigte Holzstäbchen durch einen Punkt makiert:

\begin{center}
\includegraphics{buffon03.eps}
\end{center}

Nun muss man noch herausfinden wo die günstigen Fälle liegen, also all jene Punkte, bei denen das zugehörige Holzstäbchen eine Linie kreuzt.
Da die halbe Länge eines Stäbchens geschickterweise $1$ beträgt, ist
die halbe horizontale Projektion \marginpar{\includegraphics{buffon04.eps}} (vgl. Abb. am Rand) des Stäbchens gleich $\cos{(\alpha)}$.

Wenn nun das Stäbchen eine Linie kreuzen soll, so muss diese halbe horizontale Projektion jedenfalls länger sein, als der Abstand zur nächsten Linie und der war ja $x$.
Die \glqq Kreuzungs-Bedingung\grqq\ lautet also:
\begin{equation*}
  x \leq \cos(\alpha).
\end{equation*}

Damit können wir nun die Menge der günstigen Fälle bildlich darstellen.
In der folgenden Abbildung enthält die schraffierte Fläche alle Punkte mit $x\leq \cos(\alpha)$:

\begin{center}
\includegraphics{buffon05.eps}
\end{center}

Die gesuchte Wahrscheinlichkeit ergibt sich nun einfach dadurch, dass man die schraffierte Fläche (die günstigen Fälle) durch die gesamte Fläche des Rechtecks (die möglichen Fälle) teilt (und sich vom Integral nicht abschrecken lässt):
\begin{equation*}
  p
  = \frac{\int_0^{\frac{\pi}{2}} \cos(\alpha) \dd\alpha}{\frac{\pi}{2}}
  = \frac{\bigl[\sin(\alpha)\bigr]_0^{\frac{\pi}{2}}}{\frac{\pi}{2}}
  = \frac{1-0}{\frac{\pi}{2}}
  = \frac{2}{\pi}.
\end{equation*}
Fertig.
\end{proof}

Jetzt stellt sich nur noch die Frage, wie gut sich diese Methode in der Praxis bewährt.
In der Literatur findet sich ein Bericht eines Herrn Lazzarini aus dem Jahr 1901, der sogar eine Maschine gebaut hat, um Stöckchen auf liniertes Papier fallen zu lassen.
Lazzarini berichtete, nach 3408 Würfen ein Näherungswert von $\pi\approx 3.1415929\ldots$ erhalten zu haben.
Doch dieser Wert ist leider viel zu gut, und die von Lazzarini angegebenen Werte führen direkt zur bekannten $\pi$-Annäherung von $\frac{355}{113}$, was den Verdacht aufdrängt, er habe etwas geschummelt.

Dies soll jedoch niemanden auf einer einsamen Insel davon abhalten, soviele Stöckchen wie möglich zu werfen, denn, dass ist das Wesen der Wahrscheinlichkeitsrechnung, je mehr Stäbchen man wirft, desto genauer
wird auch der Näherungswert von $\pi$ sein.
Am besten lassen wir uns dies in den Worten von Walter Faber erklären, nicht zuletzt weil Max Frischs berühmter Roman \emph{Homo Faber} einen ganz ausgezeichneten Kandidaten für ein \glqq Insel-Buch\grqq\ darstellt:

\begin{quote}
  \glqq Ich glaube nicht an Fügung und Schicksal, als Techniker bin ich gewohnt mit den Formeln der Wahrscheinlichkeit zu rechnen. [...]

  Das Wahrscheinliche (daß bei $6 000 000 000$ Würfen mit einem regelmäßigem Sechserwürfel annähernd $1 000 000 000$ Einser vorkommen) und das Unwahrscheinliche (daß bei $6$ Würfen mit demselben Würfel einmal $6$ Einser vorkommen) unterscheiden sich nicht dem Wesen nach, sondern nur der Häufigkeit nach, wobei das Häufigere von vornherein als glaubwürdiger erscheint.
  Es ist aber, wenn einmal das Unwahrscheinliche eintritt, nichts Höheres dabei, keinerlei Wunder oder Derartiges, wie es der Laie so gerne haben möchte.
  Indem wir vom Wahrscheinlichen sprechen, ist ja das Unwahrscheinliche immer schon einbegriffen und zwar als Grenzfall des Möglichen, und wenn es einmal eintritt, das Unwahrscheinliche, so besteht für unsereinen keinerlei Grund zur Verwunderung, zur Erschütterung, zur Mystifikation.\grqq
\end{quote}


Mir bleibt nur mehr allen einen schönen $\pi$-Approximation Day zu wünschen und den Hinweis anzubringen, dass derartige inoffizielle mathematische Feiertage nicht allzu ernst zu nehmen sind.

Falls jemand fand, es war verschwendete Zeit bis hier her zu lesen, so darf man sich bei mir unter \href{mailto:gsa@posteo.de}{gsa@posteo.de} gerne beschweren.

\begin{thebibliography}{10}
\bibitem{botp}
  {\sc Martin Aigner und Günther M. Ziegler:}
  {\em Das Buch der Beweise.}
  \newblock
  {Springer Verlag Berlin Heidelberg, 2002.}
\end{thebibliography}

\end{document}
